﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PlayerMovement : MonoBehaviour {


    public float speed = 5f;
	


	// Use this for initialization
	void Start () {
        
	}

    // Update is called once per frame
    void FixedUpdate () {
        this.transform.position += new Vector3(Input.GetAxis("Horizontal") * Time.fixedDeltaTime * speed, Input.GetAxis("Vertical") * Time.fixedDeltaTime * speed, 0);
       
	}
}
